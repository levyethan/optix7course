// ======================================================================== //
// Copyright 2018-2019 Ingo Wald                                            //
//                                                                          //
// Licensed under the Apache License, Version 2.0 (the "License");          //
// you may not use this file except in compliance with the License.         //
// You may obtain a copy of the License at                                  //
//                                                                          //
//     http://www.apache.org/licenses/LICENSE-2.0                           //
//                                                                          //
// Unless required by applicable law or agreed to in writing, software      //
// distributed under the License is distributed on an "AS IS" BASIS,        //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. //
// See the License for the specific language governing permissions and      //
// limitations under the License.                                           //
// ======================================================================== //

// common gdt helper tools

//note to self: // for commenting
#include "gdt/gdt.h" // 
#include "optix7.h" // opens up the optix7 library

/*! \namespace osc - Optix Siggraph Course */
namespace osc {

  /*! helper function that initializes optix and checks for errors */
  void initOptix() //sets up initOptix() as a function
  {
    // -------------------------------------------------------
    // check for available optix7 capable devices
    // -------------------------------------------------------
    cudaFree(0); // cudaFree changes no memory bcuz 0, but if a failure occurs it will let us know
    int numDevices; //set up a variable for the number of devices
    cudaGetDeviceCount(&numDevices); //gets the number of devices 
    if (numDevices == 0) //if it finds no devices
      throw std::runtime_error("#osc: no CUDA capable devices found!"); //tell us that no Devices were found
      //notice how there is no end like matlab... C++ uses } as an end 
    std::cout << "#osc: found " << numDevices << " CUDA devices" << std::endl; //if there are devices, display how many

    // -------------------------------------------------------
    // initialize optix
    // -------------------------------------------------------
    OPTIX_CHECK( optixInit() ); // initializes Optix
  }

  
  /*! main entry point to this example - initially optix, print hello
    world, then exit */
  extern "C" int main(int ac, char **av) //makes this function have C instructions
  //note: this document is in C++ but this statement allows us to go down to C
  {
    try { //try can be used to see if there are any errors
      std::cout << "#osc: initializing optix..." << std::endl; //tries to display out that statement
      
      initOptix(); //initializes optix
      
      std::cout << GDT_TERMINAL_GREEN 
                << "#osc: successfully initialized optix... yay!"
                << GDT_TERMINAL_DEFAULT << std::endl;

      // for this simple hello-world example, don't do anything else
      // ...
      std::cout << "#osc: done. clean exit." << std::endl;
      
    } catch (std::runtime_error& e) {
      std::cout << GDT_TERMINAL_RED << "FATAL ERROR: " << e.what()
                << GDT_TERMINAL_DEFAULT << std::endl;
      exit(1);
    }
    return 0;
  }
  
}
